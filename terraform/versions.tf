terraform {
  required_version = ">= 0.12.6"
  
  backend "http"{
    address = "https://gitlab.com/api/v4/projects/24526762/terraform/state/ec2_state"
    lock_address="https://gitlab.com/api/v4/projects/24526762/terraform/state/ec2_state/lock"
    unlock_address="https://gitlab.com/api/v4/projects/24526762/terraform/state/ec2_state/lock"
    lock_method="POST"
    unlock_method="DELETE"
    retry_wait_min=5
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

}
