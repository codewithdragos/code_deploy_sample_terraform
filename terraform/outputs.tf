output bucket_name {
  value = aws_s3_bucket.b.id
}

output instance_ip {
  value = aws_instance.main.public_ip
}

output "private_key" {
  value = tls_private_key.deploy.private_key_pem
  sensitive = true
}