provider "aws" {
  region = "eu-west-1"
}

data "aws_ami" "amzn-linux" {
  most_recent = true

  owners = ["amazon"]
  
  filter {
    name = "name"
    values = ["amzn-ami-*-x86_64-gp2"]
  }
}
