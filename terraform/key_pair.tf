resource "tls_private_key" "deploy" {
  algorithm = "RSA"
}

module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"
  key_name   = "deployer-one"
  public_key = tls_private_key.deploy.public_key_openssh
}